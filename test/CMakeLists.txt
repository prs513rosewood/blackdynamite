set(BLACKDYNAMITE_TEST_BD_HOST "lsmssrv1" CACHE STRING "Postgres SQL server containing the test database")
set(BLACKDYNAMITE_TEST_BD_DATABASE "" CACHE STRING "Postgres SQL test database")

configure_file(createStudy.py.cmake ${CMAKE_CURRENT_BINARY_DIR}/createStudy.py)
configure_file(fillStudy.py.cmake ${CMAKE_CURRENT_BINARY_DIR}/fillStudy.py)
configure_file(test_blackdynamite.sh.cmake ${CMAKE_CURRENT_BINARY_DIR}/test_blackdynamite.sh)

include_directories(${PROJECT_SOURCE_DIR}/src)

add_executable(test_blackdynamite test_blackdynamite.cc)
target_link_libraries(test_blackdynamite blackdynamite)

add_test(blackdynamite ${CMAKE_CURRENT_BINARY_DIR}/test_blackdynamite.sh)