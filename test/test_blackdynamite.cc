#include <blackdynamite.hh>

#include <vector>
#include <limits>

using namespace BlackDynamite;

int main(int argc, char *argv[]) {
  BlackDynamite::RunManager run_manager;

  run_manager.setNbRetriesOnRequests(10);

  run_manager.startRun();

  run_manager.push(5., "my_real", 0);
  run_manager.push(10, "my_int", 1);

  std::vector<double> vr(4);
  vr[0] = 0.; vr[1] = 1.; vr[2] = 2.; vr[3] = 3.;
  run_manager.push(vr, "my_vector_of_real", 2);

  vr[0] = std::numeric_limits<double>::quiet_NaN();
  run_manager.push(vr, "my_vector_of_real", 3);

  vr[0] = std::numeric_limits<double>::infinity();
  run_manager.push(vr, "my_vector_of_real", 4);

  std::vector<int> vi(3);
  vi[0] = 10; vi[1] = 25; vi[2] = 15;
  run_manager.push(vi, "my_vector_of_int", 3);

  run_manager.push("pouet", "value1");

  run_manager.endRun();

  return 0;
}
