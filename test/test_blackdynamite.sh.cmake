#!/bin/bash

export PYTHONPATH=@PROJECT_SOURCE_DIR@/python:$PYTHONPATH

export BLACKDYNAMITE_HOST=@BLACKDYNAMITE_TEST_BD_HOST@
export MACHINE_NAME=$(hostname)
export BLACKDYNAMITE_STUDY=test_blackdynamite

@CMAKE_CURRENT_BINARY_DIR@/createStudy.py  --truerun --yes
if [ ! $? -eq 0 ]; then
    exit 1
fi

@CMAKE_CURRENT_BINARY_DIR@/fillStudy.py --machine_name $MACHINE_NAME --nproc 1 --run_name test --host @BLACKDYNAMITE_TEST_BD_HOST@ --truerun
if [ ! $? -eq 0 ]; then
    exit 2
fi

@PROJECT_SOURCE_DIR@/bin/launchRuns.py --machine_name $MACHINE_NAME --outpath @CMAKE_CURRENT_BINARY_DIR@ --truerun --stdout --stop_on_error
if [ ! $? -eq 0 ]; then
    exit 3
fi

@PROJECT_SOURCE_DIR@/bin/cleanRuns.py --delete --yes --truerun
if [ ! $? -eq 0 ]; then
    exit 4
fi

