#!/usr/bin/env python
import BlackDynamite as BD

parser = BD.BDParser()
params = parser.parseBDParameters()
base = BD.Base(**params)

#creation of job description
myjob_desc = BD.Job(base)
myjob_desc.types["param1"] = int
myjob_desc.types["param2"] = int

 #creation of run description
myruns_desc = BD.Run(base)
myruns_desc.types["value1"] = str
myruns_desc.types["value2"] = str

 #creation de la base
quantities = {}
base.createBase(myjob_desc,
                myruns_desc,
                quantities,
                **params)

