#!/usr/bin/env python
import BlackDynamite as BD

print BD.__path__

parser = BD.RunParser()
params = parser.parseBDParameters()
base = BD.Base(**params)

# Create jobs

jobs = BD.Job(base)
jobs["param1"] = [1]
jobs["param2"] = [10]

base.createParameterSpace(jobs)

# Create Runs

myrun = BD.Run(base)
myrun.setEntries(params)

jobSelector = BD.JobSelector(base)
if ("job_constraints" not in params):
    params["job_constraints"] = []

job_list = jobSelector.selectJobs(params["job_constraints"])

myrun.execfile = BD.conffile.addFile("launch.sh", base,
                                     content="""
@CMAKE_CURRENT_BINARY_DIR@/test_blackdynamite __BLACKDYNAMITE__run_id__
""")

myrun["exec"] = myrun.execfile.id

myrun["value1"] = "toto"
myrun["value2"] = "second value"

for j in job_list:
    myrun.attachToJob(j)
    print (j.entries)
    print (myrun.entries)


if ("truerun" in params):
    base.commit()
