/*
  author : Nicolas RICHART <nicolas.richart@epfl.ch>
*/

#include "state_manager.hh"
#include "common_types.hh"
#include "sql_connection_manager.hh"
#include "sql_query.hh"

namespace BlackDynamite {
class StateManager::StateUpdater : public SQLQuery<bd_transaction> {
public:
  StateUpdater(const std::string & state, UInt run_id)
    : SQLQuery<bd_transaction>("update_state"), state(state), run_id(run_id){};

protected:
  pqxx::result execute(pqxx::transaction_base & trans) {
    // if (this->state == RunManager::STARTED) {
    //   pqxx::result r = trans.prepared("get_state").exec();

    //   if(r.empty()) FATAL("Run "  << this->run_id << " does not exist!");

    //   std::string get_state;
    //   r[0][0].to(get_state);
    //   if (get_state == "START")
    //     FATAL("Run " << this->run_id
    //           << " was already previously ran: abort");
    // }

    // std::string new_state;
    // switch (this->state){
    // case RunManager::STARTED: new_state = "START"; break;
    // case RunManager::ENDED:   new_state = "FINISHED";break;
    // default: FATAL("this should not append");
    // }

    //      trans.prepared("update_state")(new_state).exec();
    return trans.prepared("update_state")(state).exec();
  }

private:
  std::string state;
  UInt run_id;
};

StateManager::StateManager(SQLConnectionManager & connection_manager,
                           const std::string & sql_schema, UInt run_id,
                           UInt request_retries)
    : connection_manager(connection_manager), run_id(run_id),
      request_retries(request_retries) {
  std::string schema = sql_schema;
  if (sql_schema != "")
    schema += ".";

  pqxx::connection_base & cnx = this->connection_manager.getConnection();

  if (this->state_manager_counter == 0) {
    std::string update_state_sql = "UPDATE " + schema + "runs" +
                                   " as r SET state = $1 WHERE id = " +
                                   cnx.quote(run_id) + ";";
    std::string get_state_sql = "SELECT state FROM " + schema + "runs" +
                                " as r WHERE r.id = " + cnx.quote(run_id) + ";";

    BLACKDYNAMITE_DEBUG("StateManager",
                        "update_state_sql -> " + update_state_sql);
    BLACKDYNAMITE_DEBUG("StateManager", "get_state_sql -> " + get_state_sql);

    SQLQueryPrepareHelper::prepare<std::string>(cnx, "update_state",
                                                update_state_sql);
    SQLQueryPrepareHelper::prepare(cnx, "get_state", get_state_sql);
  }

  ++this->state_manager_counter;
}

StateManager::~StateManager() {
  --this->state_manager_counter;
  if (state_manager_counter == 0) {
    pqxx::connection_base & cnx = this->connection_manager.getConnection();
    SQLQueryPrepareHelper::unprepare(cnx, "update_state");
    SQLQueryPrepareHelper::unprepare(cnx, "get_state");
  }
}

void StateManager::changeState(const std::string & state) {
  this->state = state;
  pqxx::connection_base & cnx = this->connection_manager.getConnection();
  StateUpdater query(state, this->run_id);
  cnx.perform(query, this->request_retries);
}

const std::string & StateManager::getState() const { return state; }

UInt StateManager::state_manager_counter = 0;
}
