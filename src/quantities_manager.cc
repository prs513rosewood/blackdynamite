#include "quantities_manager.hh"
#include "sql_connection_manager.hh"
#include "common_types.hh"
#include "sql_query.hh"

namespace BlackDynamite {

QuantitiesManager::QuantitiesManager(SQLConnectionManager & connection_manager,
                                     const std::string & sql_schema,
                                     UInt run_id, UInt request_retries)
    : connection_manager(connection_manager), run_id(run_id),
      request_retries(request_retries) {
  this->schema = sql_schema;
  if (sql_schema != "")
    this->schema += ".";

  pqxx::connection_base & cnx = this->connection_manager.getConnection();

  if (this->quantities_manager_counter == 0) {
    std::string get_quantities_sql =
        "SELECT q.id, q.name FROM " + schema + "quantities" + " as q;";
    std::string get_quantity_sql = "SELECT q.id FROM " + schema + "quantities" +
                                   " as q WHERE q.name = $1;";
    std::string create_quantity_sql =
        "INSERT INTO " + schema + "quantities (name, is_integer, is_vector) "
                                  "VALUES ($1, $2, $3) RETURNING id;";
    std::string push_scalar_integer_sql =
        "INSERT INTO " + schema +
        "scalar_integer (run_id, quantity_id, measurement, step) VALUES (" +
        cnx.quote(run_id) + ", $1, $2, $3);";
    std::string push_scalar_real_sql =
        "INSERT INTO " + schema +
        "scalar_real (run_id, quantity_id, measurement, step) VALUES (" +
        cnx.quote(run_id) + ", $1, $2, $3);";
    std::string push_vector_integer_sql =
        "INSERT INTO " + schema +
        "vector_integer (run_id, quantity_id, measurement, step) VALUES (" +
        cnx.quote(run_id) + ", $1, $2, $3);";
    std::string push_vector_real_sql =
        "INSERT INTO " + schema +
        "vector_real (run_id, quantity_id, measurement, step) VALUES (" +
        cnx.quote(run_id) + ", $1, $2, $3);";

    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "get_quantities -> " + get_quantities_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "get_quantity -> " + get_quantity_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "create_quantity -> " + create_quantity_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "push_scalar_integer -> " + push_scalar_integer_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "push_scalar_real -> " + push_scalar_real_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "push_vector_integer -> " + push_vector_integer_sql);
    BLACKDYNAMITE_DEBUG("QuantitiesManager",
                        "push_vector_real -> " + push_vector_real_sql);

    SQLQueryPrepareHelper::prepare(cnx, "get_quantities", get_quantities_sql);
    SQLQueryPrepareHelper::prepare<std::string>(cnx, "get_quantity",
                                                get_quantity_sql);
    SQLQueryPrepareHelper::prepare<std::string, bool, bool>(
        cnx, "create_quantity", create_quantity_sql);
    SQLQueryPrepareHelper::prepare<UInt, UInt, UInt>(cnx, "push_scalar_integer",
                                                     push_scalar_integer_sql);
    SQLQueryPrepareHelper::prepare<UInt, Real, UInt>(cnx, "push_scalar_real",
                                                     push_scalar_real_sql);
    SQLQueryPrepareHelper::prepare<UInt, std::vector<UInt>, UInt>(
        cnx, "push_vector_integer", push_vector_integer_sql);
    SQLQueryPrepareHelper::prepare<UInt, std::vector<Real>, UInt>(
        cnx, "push_vector_real", push_vector_real_sql);
  }

  ++this->quantities_manager_counter;
  QueryQuantitiesRetriever quantities_retriever(this->quantities);
  cnx.perform(quantities_retriever, this->request_retries);
}

QuantitiesManager::~QuantitiesManager() {
  --this->quantities_manager_counter;
  if (this->quantities_manager_counter == 0) {
    pqxx::connection_base & cnx = this->connection_manager.getConnection();
    SQLQueryPrepareHelper::unprepare(cnx, "get_quantities");
    SQLQueryPrepareHelper::unprepare(cnx, "get_quantity");
    SQLQueryPrepareHelper::unprepare(cnx, "create_quantity");
    SQLQueryPrepareHelper::unprepare(cnx, "push_scalar_integer");
    SQLQueryPrepareHelper::unprepare(cnx, "push_scalar_real");
    SQLQueryPrepareHelper::unprepare(cnx, "push_vector_integer");
    SQLQueryPrepareHelper::unprepare(cnx, "push_vector_real");
  }
}

void QuantitiesManager::pushQuantity(const std::string & name,
                                     const std::string & value) {
  pqxx::connection_base & cnx = this->connection_manager.getConnection();
  SQLQuery<bd_transaction> query("UPDATE " + this->schema + "runs SET " + name +
                                     " = " + cnx.quote(value) + " WHERE id = " +
                                     cnx.quote(this->run_id) + ";",
                                 "update_quantity");
  cnx.perform(query, this->request_retries);
}

UInt QuantitiesManager::quantities_manager_counter = 0;
}
