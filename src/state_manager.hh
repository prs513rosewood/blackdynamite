#ifndef __BLACKDYNAMITE_STATE_UPDATER_HH__
#define __BLACKDYNAMITE_STATE_UPDATER_HH__
/* -------------------------------------------------------------------------- */
#include "blackdynamite.hh"
/* -------------------------------------------------------------------------- */

namespace BlackDynamite {
class SQLConnectionManager;

class StateManager {
public:
  StateManager(SQLConnectionManager & connection_manager,
               const std::string & sql_schema, UInt run_id,
               UInt request_retries = 10);
  ~StateManager();

  void changeState(const std::string & state);
  const std::string & getState() const;

private:
  class StateUpdater;

protected:
  SQLConnectionManager & connection_manager;
  std::string state;
  UInt run_id;
  static UInt state_manager_counter;
  UInt request_retries;
};
}
#endif /* __BLACKDYNAMITE_STATE_UPDATER_HH__ */
