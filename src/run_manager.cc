/*
  author : Nicolas RICHART <nicolas.richart@epfl.ch>
  author : Guillaume ANCIAUX (anciaux@labri.fr, g.anciaux@laposte.net)
  author : Till JUNGE <till.junge@epfl.ch>
*/

/* -------------------------------------------------------------------------- */
#include <pqxx/pqxx>

#include "blackdynamite.hh"

#include "sql_connection_manager.hh"

#include "state_manager.hh"
#include "quantities_manager.hh"

#include <sstream>
#include <fstream>
#include <cmath>

/* -------------------------------------------------------------------------- */
namespace BlackDynamite {
/* ------------------------------------------------------------------------ */
static std::string getenv(const std::string & var, std::string & default_val) {
  char * ptr = ::getenv(var.c_str());
  if (!ptr)
    return default_val;
  return std::string(ptr);
}

static std::string getenv(const std::string & var) {
  char * ptr = ::getenv(var.c_str());
  if (!ptr)
    FATAL("Undefined environment variable " + var);
  return std::string(ptr);
}
/* ------------------------------------------------------------------------ */

std::string RunManager::readPassword(const std::string & host) {
  std::string home = getenv("HOME");
  std::string host_conf = home + "/.blackdynamite/" + host + ".bd";
  std::ifstream fin(host_conf.c_str());
  std::string line;
  std::string password = "";

  //    std::cerr << "try to read the password" << std::endl;

  while (fin.good()) {
    getline(fin, line);

    std::stringstream sstr(line);
    std::vector<std::string> tokens;
    std::copy(std::istream_iterator<std::string>(sstr),
              std::istream_iterator<std::string>(), std::back_inserter(tokens));

    if (tokens.size() == 3) {
      if (tokens[0] == "password" && tokens[1] == "=") {
        password = tokens[2];
        break;
      }
    }
  }
  //    std::cerr << "I have read the password: " << password << std::endl;
  std::cerr << "I have read the password" << std::endl;
  return password;
}

/* ------------------------------------------------------------------------ */

RunManager::RunManager() {
  std::string user = getenv("BLACKDYNAMITE_USER");
  std::string dbname = getenv("BLACKDYNAMITE_DBNAME", user);
  std::string host = getenv("BLACKDYNAMITE_HOST");
  std::string schema = getenv("BLACKDYNAMITE_SCHEMA");
  UInt run_id = atoi(getenv("BLACKDYNAMITE_RUN_ID").c_str());

  std::string password = readPassword(host);

  init(dbname, user, host, schema, password, run_id);
}

/* ------------------------------------------------------------------------ */
RunManager::RunManager(const std::string & dbname, const std::string & user,
                       const std::string & host, const std::string & schema,
                       const std::string & password, UInt run_id) {
  init(dbname, user, host, schema, password, run_id);
}

/* ------------------------------------------------------------------------ */
RunManager::~RunManager() {
  delete quantities_manager;
  delete state_manager;
}

/* ------------------------------------------------------------------------ */
void RunManager::init(const std::string & dbname, const std::string & user,
                      const std::string & host, const std::string & schema,
                      const std::string & password, UInt run_id) {
  this->request_retries = 10;

  this->run_id = run_id;
  if (this->run_id < 1) {
    FATAL("The run id required for the sql dumps has not been specified");
  }

  this->sql_connection_manager =
      new SQLConnectionManager(dbname, user, host, schema, password);

  this->sql_schema = schema;
  this->quantities_manager =
      new QuantitiesManager(*this->sql_connection_manager, this->sql_schema,
                            this->run_id, this->request_retries);

  this->state_manager =
      new StateManager(*this->sql_connection_manager, this->sql_schema,
                       this->run_id, this->request_retries);
}

/* ------------------------------------------------------------------------ */
void RunManager::startRun() { this->state_manager->changeState("START"); }

/* ------------------------------------------------------------------------ */
void RunManager::endRun() { this->state_manager->changeState("FINISHED"); }

/* ------------------------------------------------------------------------ */
void RunManager::changeState(const std::string & state) {
  this->state_manager->changeState(state);
}

/* ------------------------------------------------------------------------ */
UInt RunManager::getInstanceCounter() {
  return this->sql_connection_manager->getInstanceCounter();
}

/* ------------------------------------------------------------------------ */
template <typename T>
void RunManager::push(const T & value, const std::string & quantity,
                      const UInt & step) {
  this->quantities_manager->pushQuantity(quantity, value, step);
}

/*------------------------------------------------------------------------- */
void RunManager::push(const std::string & value, const std::string & quantity) {
  this->quantities_manager->pushQuantity(quantity, value);
}

/* ------------------------------------------------------------------------ */
template void RunManager::push<int>(const int & value,
                                    const std::string & quantity,
                                    const UInt & step);
template void RunManager::push<UInt>(const UInt & value,
                                     const std::string & quantity,
                                     const UInt & step);
template void RunManager::push<Real>(const Real & value,
                                     const std::string & quantity,
                                     const UInt & step);
template void
RunManager::push<std::vector<Real> >(const std::vector<Real> & value,
                                     const std::string & quantity,
                                     const UInt & step);
template void
RunManager::push<std::vector<int> >(const std::vector<int> & value,
                                    const std::string & quantity,
                                    const UInt & step);

/* ------------------------------------------------------------------------ */
unsigned int SQLConnectionManager::connection_counter = 0;
pqxx::connection * SQLConnectionManager::sql_connection = NULL;
/* ------------------------------------------------------------------------ */
}
