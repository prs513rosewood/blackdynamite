/*
  author : Nicolas RICHART <nicolas.richart@epfl.ch>
*/

#ifndef __BLACKDYNAMITE_COMMON_TYPES_HH__
#define __BLACKDYNAMITE_COMMON_TYPES_HH__

#include <pqxx/pqxx>
#include <type_traits>
#include <vector>

/* -------------------------------------------------------------------------- */
#include <iostream>
#include <cstdlib>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#define FATAL(x) {std::cerr << x << std::endl ; exit(EXIT_FAILURE);}

#ifndef BLACKDYNAMITE_NDEBUG
#include <iomanip>
#define BLACKDYNAMITE_DEBUG(module, msg)        \
  do {                                          \
    std::cerr << std::setw(7) << module << ": " \
              << msg << std::endl;              \
  } while(0)
#else
#define BLACKDYNAMITE_DEBUG(module, msg)
#endif

/* -------------------------------------------------------------------------- */
namespace BlackDynamite {
  // pqxx transaction type
  typedef pqxx::robusttransaction<pqxx::serializable> bd_transaction;

  template<typename>               struct is_std_vector : std::false_type {};
  template<typename T, typename A> struct is_std_vector<std::vector<T, A>> : std::true_type {};

  template<typename>               struct is_vector_of_integer : std::false_type {};
  template<typename T, typename A> struct is_vector_of_integer<std::vector<T, A>> : std::is_integral<T> {};

}
#endif /* __BLACKDYNAMITE_COMMON_TYPES_HH__ */
