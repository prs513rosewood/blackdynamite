#ifndef __BLACKDYNAMITE_SQL_PUSHER_HH__
#define __BLACKDYNAMITE_SQL_PUSHER_HH__
/* -------------------------------------------------------------------------- */
#include "pusher.hh"
#include <cmath>
/* -------------------------------------------------------------------------- */


namespace BlackDynamite {

  template<typename type, bool is_scalar >
  class Pusher::SqlPusher : public pqxx::transactor<transaction> {
    typedef transaction_input<type, is_scalar> InputObject;

    /* ------------------------------------------------------------------------ */
    /* Constructors/Destructors                                                 */
    /* ------------------------------------------------------------------------ */
    
  public:
    SqlPusher(InputObject & input)
    :pqxx::transactor<transaction>("Inserting into table" + input.tablename),
     input(input) {}
    
    /* ------------------------------------------------------------------------ */
    /* Accessors                                                                */
    /* ------------------------------------------------------------------------ */
    
    void operator()(transaction & T);

    /* ------------------------------------------------------------------------ */
    /* Class Members                                                            */
    /* ------------------------------------------------------------------------ */
    
  private:
    InputObject & input;
  };

  /* -------------------------------------------------------------------------- */
  
  //partial specialization when scalartype 
  
  template<typename type>
  class Pusher::SqlPusher<type, true>: public pqxx::transactor<transaction> {
    typedef Pusher::transaction_input<type, true> InputObject;

    /* ------------------------------------------------------------------------ */
    /* Constructors/Destructors                                                 */
    /* ------------------------------------------------------------------------ */
    
  public:
  SqlPusher(InputObject & input)
    :pqxx::transactor<transaction>("Inserting into table" + input.tablename),
     input(input)
  {}

    /* ------------------------------------------------------------------------ */
    /* Accessors                                                                */
    /* ------------------------------------------------------------------------ */
    
    void operator()(transaction & T);

    /* ------------------------------------------------------------------------ */
    /* Class Members                                                            */
    /* ------------------------------------------------------------------------ */
    
  private:
    InputObject & input;
  };

/* -------------------------------------------------------------------------- */
#include "sql_pusher_inline_impl.cc"
/* -------------------------------------------------------------------------- */
  

}



#endif /* __BLACKDYNAMITE_SQL_PUSHER_HH__ */
