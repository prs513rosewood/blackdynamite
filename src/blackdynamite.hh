/*
  author : Nicolas RICHART <nicolas.richart@epfl.ch>
  author : Guillaume ANCIAUX (anciaux@labri.fr, g.anciaux@laposte.net)
  author : Till JUNGE <till.junge@epfl.ch>
*/

/* -------------------------------------------------------------------------- */
#ifndef __BLACKDYNAMITE_PUSHER_HH__
#define __BLACKDYNAMITE_PUSHER_HH__

#include <string>

/* -------------------------------------------------------------------------- */
namespace BlackDynamite {
typedef unsigned int UInt;
typedef double Real;

class SQLConnectionManager;
class StateManager;
class QuantitiesManager;

class RunManager {
  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  RunManager();
  RunManager(const std::string & dbname, const std::string & user,
             const std::string & host, const std::string & schema,
             const std::string & password, UInt run_id);

  virtual ~RunManager();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
  //! function to be called to update the state of the job at the begining of
  // the run
  void startRun();

  //! function to be called to update the state of the job at the end of the run
  void endRun();

  //! change the state of a run
  void changeState(const std::string & state);

  //! template method that pushes a value associated with a quantity at a
  // particular timestep
  template <typename T>
  void push(const T & value, const std::string & quantity, const UInt & step);

  //! template method that pushes a string to a given run column
  void push(const std::string & value, const std::string & quantity);

  //! give the number of instance of blackdynamite
  UInt getInstanceCounter();

  /// defines how many times requests should be retired on failure
  UInt setNbRetriesOnRequests(UInt retries) { this->request_retries = retries; }

protected:
  //! initialisation method
  void init(const std::string & dbname, const std::string & user,
            const std::string & host, const std::string & schema,
            const std::string & password, UInt run_id);

  //! read the password from .blacdynamite directory
  std::string readPassword(const std::string & host);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
protected:
  UInt run_id;
  std::string sql_schema;

  SQLConnectionManager * sql_connection_manager;
  StateManager * state_manager;
  QuantitiesManager * quantities_manager;

  UInt request_retries;
};

/* -------------------------------------------------------------------------- */
}

#endif /* __BLACKDYNAMITE_PUSHER_HH__ */
