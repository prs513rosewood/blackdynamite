#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

try:
    import unittest
    import os
    import pep8
    import BlackDynamite

except ImportError as err:
    import sys
    print(err)
    sys.exit(str(err))


class Pep8Test(unittest.TestCase):
    """
    Test for pep8 conformity
    """
    def setUp(self):
        """
        builds a list of source files. If you find a smarter way, please let me
        know
        """
        print()  # for emacs to evaluate the first line of errors
        self.mod_files = list()
        bd_path = os.path.join(BlackDynamite.__path__[0], "..")
        for dirpath, _, filenames in os.walk(bd_path):
            self.mod_files += [os.path.join(dirpath, filename)
                               for filename in filenames
                               if filename.endswith((".py", ".pyx"))]

        for dirpath, _, filenames in os.walk(os.path.join(bd_path, 'scripts')):
            self.mod_files += [os.path.join(dirpath, filename)
                               for filename in filenames
                               if not filename.endswith(".sh")]

    def test_pep8_conformity(self):
        """
        check all files for pep8 conformity
        """
        pep8style = pep8.StyleGuide()
        pep8style.check_files((mod_file for mod_file in self.mod_files))
