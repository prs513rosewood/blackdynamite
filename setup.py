# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

setup(name="blackdynamite",
      packages=['BlackDynamite', 'BlackDynamite.coating'],
      version="0.0.1",
      author="Guillaume Anciaux",
      author_email="guillaume.anciaux@epfl.ch",
      description=("Compliant parametric study tool"),
      package_data={
          'BlackDynamite': ['build_tables.sql']},
      scripts=['scripts/canYouDigIt.py',
               'scripts/cleanRuns.py',
               'scripts/createUser.py',
               'scripts/enterRun.py',
               'scripts/getRunInfo.py',
               'scripts/launchRuns.py',
               'scripts/mvRuns.py',
               'scripts/pushQuantity.py',
               'scripts/saveBDStudy.py',
               'scripts/updateRuns.py',
               'scripts/studyInfo.py',
               'scripts/bash_completion.sh'],
      install_requires=["psycopg2-binary", "psycopg2", "numpy",
                        "argcomplete", "pyparsing"],
      test_suite="pythontests",
      license="""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
""")
