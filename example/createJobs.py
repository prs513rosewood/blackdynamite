#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# First we need to set the python headers
# and to import the \blackdynamite modules
import BlackDynamite as BD

# Then you have to create a generic black dynamite parser
# and parse the system (including the connection parameters and credentials)
parser = BD.bdparser.BDParser()
params = parser.parseBDParameters()

# Then we can connect to the black dynamite database
base = BD.base.Base(**params)

# create of job object
job = BD.job.Job(base)

# specify a range of jobs
job["param1"] = 10
job["param2"] = [3.14, 1., 2.]
job["param3"] = 'toto'

# creation of the jobs on the database
base.createParameterSpace(job)
