#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# First we need to set the python headers and
# to import the blackdynamite modules
import BlackDynamite as BD

# Then you have to create a generic black dynamite parser
# and parse the system (including the connection parameters and credentials)
parser = BD.bdparser.BDParser()
params = parser.parseBDParameters()

# Then we can connect to the black dynamite database
base = BD.base.Base(**params, creation=True)

# Then you have to define the parametric space (the job pattern)
myjob_desc = BD.job.Job(base)
myjob_desc.types["param1"] = int
myjob_desc.types["param2"] = float
myjob_desc.types["param3"] = str

# Then you have to define the run pattern
myruns_desc = BD.run.Run(base)
myruns_desc.types["compiler"] = str

# Then we request for the creation of the database
base.createBase(myjob_desc, myruns_desc, **params)
