#!/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################
import BlackDynamite as BD
import matplotlib.pyplot as plt
################################################################
# basic connection
parser = BD.BDParser()
params = parser.parseBDParameters(
    '--host lsmssrv1.epfl.ch --study mystudy'.split())
mybase = BD.Base(**params)
################################################################


# function to plot things (user's job)
def plot(run_list):
    for r, j in run_list:
        ekin = r.getScalarQuantity('ekin')
        if ekin is None:
            continue
        print(j)
        list_files = r.listFiles()
        print(list_files)
        fname = r.getFile(list_files[3])
        print(fname + ':')
        _file = open(fname)
        print(_file.read())
        plt.plot(ekin[:, 0], ekin[:, 1], 'o-',
                 label='$p_2 = {0}$'.format(j['param2']))

    plt.legend(loc='best')
    plt.show()
################################################################


# selecting some runs
runSelector = BD.RunSelector(mybase)
run_list = runSelector.selectRuns(params, params)
plot(run_list)

# selecting some other runs
params['run_constraints'] = ['run_name =~ test', 'state = FINISHED']
params['job_constraints'] = ['param2 > 1']
run_list = runSelector.selectRuns(params, params)
plot(run_list)
