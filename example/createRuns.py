#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# First we need to set the python headers
# and to import the blackdynamite modules
import BlackDynamite as BD

# import a runparser (instead of a generic BD parser)
parser = BD.RunParser()
params = parser.parseBDParameters()

# Then we can connect to the black dynamite database
base = BD.Base(**params)

# create a run object
myrun = BD.Run(base)

# set the run parameters from the parsed entries
myrun.setEntries(params)

# add a configuration file
myrun.addConfigFiles("doIt.py")

# set the entry point (executable) file
myrun.setExecFile("launch.sh")

# create a job selector
jobSelector = BD.JobSelector(base)

# select the jobs that should be associated with the runs about to be created
job_list = jobSelector.selectJobs(params)

# create the runs
for j in job_list:
    myrun['compiler'] = 'gcc'
    myrun.attachToJob(j)

# if truerun, commit the changes to the base
if (params["truerun"] is True):
    base.commit()
