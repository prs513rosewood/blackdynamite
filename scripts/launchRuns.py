#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################
import BlackDynamite as BD
import os
import sys
import socket
from types import ModuleType
################################################################


def main(argv=None):
    if (type(argv) == str):
        argv = argv.split()

    parser = BD.BDParser()
    parser.register_params(
        group="launchRuns.py",
        params={
            "outpath": str,
            "generator": ModuleType,
            "nruns": int,
            "state": str,
            "machine_name": str},
        defaults={
            "machine_name": socket.gethostname(),
            "nruns": -1,
            "generator": "bashCoat"})

    parser.help.update({
        "nruns": ('Specify the number of runs to launch. '
                  'This is useful when we want to launch '
                  'only the first run from the stack.'),
        "generator": "Specify the launcher generator"
        })

    params = parser.parseBDParameters(argv=argv)
    mybase = BD.Base(**params)

    if ("outpath" not in params):
        print('A directory where to create temp files '
              'should be provided. use --outpath ')
        sys.exit(-1)

    mydir = os.path.join(params["outpath"], "BD-" + params["study"] + "-runs")
    if not os.path.exists(mydir):
        os.makedirs(mydir)

    os.chdir(mydir)

    runSelector = BD.RunSelector(mybase)
    constraints = []
    if ("constraints" in params):
        constraints = params["constraints"]

    def item_matcher(name, item):
        return item.lower().lstrip().startswith(name)

    if not any([item_matcher("state", item) for item in constraints]):
        constraints.append("state = CREATED")
    if not any([item_matcher("machine_name", item)
                for item in constraints]):
        constraints.append("machine_name = {0}".format(
            params["machine_name"]))

    run_list = runSelector.selectRuns(constraints)

    if (params["nruns"] > 0):
        run_list = [run_list[i]
                    for i in range(0, min(params["nruns"],
                                          len(run_list)))]

    if (len(run_list) == 0):
        print("No runs to be launched")

    for r, j in run_list:
        print("Dealing with job {0.id}, run {1.id}".format(j, r))
        r["run_path"] = os.path.join(mydir, "run-" + str(r.id))
        print(j.types)
        j.update()

        if not os.path.exists("run-" + str(r.id)):
            os.makedirs("run-" + str(r.id))

        os.chdir("run-" + str(r.id))

        conffiles = r.getConfigFiles()
        for conf in conffiles:
            print("create file " + conf["filename"])
            f = open(conf["filename"], 'w')
            f.write(conf["file"])
            f.close()

        print("launch in '" + mydir + "/" + "run-" + str(r.id) + "/'")
        mymod = params["generator"]
        print(mymod)
        mymod.launch(r, params)

        os.chdir("../")

    if (params["truerun"] is True):
        mybase.commit()


if __name__ == '__main__':
        main()
