#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import BlackDynamite as BD
import sys
import datetime
################################################################


def printSummary(mybase, params):
    runSelector = BD.RunSelector(mybase)
    run_list = runSelector.selectRuns(params, quiet=True)
    print("*"*6 + " run summary => study {0} ".format(mybase.schema) + "*"*6)
    if len(run_list) == 0:
        print("no runs found")
        sys.exit(0)

    request = "SELECT run_name,state,count(state) from {0}.runs ".format(
        mybase.schema)
    if (len(run_list) > 0):
        request += "where id in (" + ",".join(
            [str(r.id) for r, j in run_list]) + ")"
    request += " group by state,run_name order by run_name,state"
    # print (request)
    curs = mybase.performRequest(request, [])
    stats = {}
    for i in curs:
        if i[0] not in stats:
            stats[i[0]] = []
        stats[i[0]].append([i[1], int(i[2])])

    for run_name, st in stats.items():
        tot = 0
        for n, count in st:
            tot += count
        for n, count in st:
            print("{:20} {:>20} => {:5} ({:>5.1f}%)".format(
                run_name, n, count, 100.*count/tot))
        print("")
    sys.exit(0)

################################################################


def getRunInfo(run_id, mybase):
    myrun = BD.Run(mybase)
    myrun["id"] = run_id
    myrun.id = run_id
    run_list = myrun.getMatchedObjectList()

    if (len(run_list) == 0):
        print("no run found with id " + str(run_id))
        sys.exit(1)

    myrun = run_list[0]
    myjob = BD.Job(mybase)

    myjob.id = myrun["job_id"]
    myjob["id"] = myrun["job_id"]
    job_list = myjob.getMatchedObjectList()

    if (len(job_list) == 0):
        print("no job found with id " + myjob.id)
        sys.exit(1)

    myjob = job_list[0]
    list_entries = myjob.entries.keys()
    print("*"*6 + " job info " + "*"*6)
    for entry in list_entries:
        if (myjob[entry]):
            print(entry + ": " + str(myjob[entry]))

    print("*"*6 + " run info " + "*"*6)
    list_entries = list(myrun.entries.keys())
    regular_run_entries = ("run_name",
                           "job_id",
                           "state",
                           "start_time",
                           "machine_name",
                           "exec",
                           "nproc",
                           "wait_id")

    for entry in regular_run_entries:
        if (myrun[entry]):
            print(entry + ": " + str(myrun[entry]))
        list_entries.remove(entry)

    for entry in list_entries:
        if (myrun[entry]):
            print(entry + ": " + str(myrun[entry]))

    conffiles = myrun.getConfigFiles()
    for conf in conffiles:
        print("*"*6)
        print("file #" + str(conf.id) + ": " + conf["filename"])
        print("*"*6)
        print(conf["file"])

################################################################


def getInfoNames():
    infos = []
    infos.append("run_name")
    infos.append("id")
    infos.append("job_id")
    if "infos" in params:
        infos += params['infos']
    else:
        infos += ["state", "nproc", "machine_name"]

    infos.append("start_time")
    infos.append("last step")
    infos.append("last update")
    infos.append("Time/step")
    infos.append("Total Time")

    return infos

################################################################


def getFormatString(infos):
    format_string = " {:<20} | {:^6} | {:^6} |"
    if "infos" in params:
        format_string += " {:^10} |" * len(params['infos'])
    else:
        format_string += " {:<15} | {:^5} | {:<20} |"
    format_string += " {:14} | {:>9} | {:>16} | {:>10} | {:>16} |"
    return format_string

################################################################


def formatTimeDelta(t):
    if (t < datetime.timedelta(seconds=1)):
        if (t < datetime.timedelta(microseconds=1000)):
            t = str(t.microseconds) + u'\u00B5'.encode('UTF-8') + "s"
        else:
            t = str(1./1000.*t.microseconds) + 'ms'
    else:
        ms = t.microseconds
        t -= datetime.timedelta(microseconds=ms)
        t = str(t)
    return t

################################################################


def getTimeInfos(r):

    step, steptime = r.getLastStep()
    start_time = r['start_time']
    time_perstep = None
    total_time = None

    if (step is not None and steptime and start_time):
        time_perstep = (steptime-start_time)/(step+1)
        total_time = steptime-start_time
        time_perstep = formatTimeDelta(time_perstep)
        total_time = formatTimeDelta(total_time)

    if start_time:
        start_time = start_time.strftime("%H:%M %d/%m/%y")
    if steptime:
        steptime = steptime.strftime("%H:%M %d/%m/%y")

    run_infos = [start_time, step, steptime, time_perstep, total_time]
    return run_infos

################################################################


def getRunInfos(r, j):
    run_infos = []
    for col in info_names[:-5]:
        key_run = col.replace('%r.', '').strip()
        if not key_run == 'start_time':
            if key_run in r.entries:
                run_infos.append(r[key_run])
            else:
                key_job = col.replace('%j.', '').strip()
                if key_job in j.entries:
                    run_infos.append(j[key_job])
                else:
                    raise Exception('Key {0} is not a valid parameter'.format(
                        key_run))
    run_infos += getTimeInfos(r)

    return run_infos
################################################################


parser = BD.BDParser()
parser.register_params(
    group="getRunInfo",
    params={"run_id": int, "order": str,
            "summary": bool,
            "infos": [str]},
    defaults={"order": "id"},
    help={"run_id": "Select a run_id for complete output",
          "summary": "Output a summary of the completeness of the study",
          "order": "specify the column which serves to order the lines"})

params = parser.parseBDParameters()
mybase = BD.Base(**params)

if params["summary"] is True:
    printSummary(mybase, params)

if ("run_id" in params):
    getRunInfo(params["run_id"], mybase)
else:

    info_names = getInfoNames()
    format_string = getFormatString(info_names)
    header = format_string.format(*info_names)
    separator = "-" * len(header)

    print(separator)
    print(header)
    print(separator)

    runSelector = BD.RunSelector(mybase)
    run_list = runSelector.selectRuns(params,
                                      sort_by="runs." + params["order"],
                                      quiet=True)
    for r, j in run_list:
        try:
            infos = getRunInfos(r, j)

            def transform_None(x):
                if x is None:
                    return 'None'
                else:
                    return x
            infos = [transform_None(x) for x in infos]
            line = format_string.format(*infos)
            print(line)
        except Exception as e:
            print(getRunInfos(r, j))
            print(e)
