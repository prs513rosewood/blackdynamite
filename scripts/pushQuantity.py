#!/usr/bin/env python3
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import BlackDynamite as BD


def main(argv=None):
    parser = BD.BDParser()
    parser.register_params(
        group="pushQuantity",
        params={"runid": int,
                "quantity_id": str,
                "value": str,
                "values": [str],
                "step": int,
                "is_float": bool},
        defaults={"step": 0},
        help={"runid": "The id of the run to update",
              "quantity_id": "ID of the Quantity to push",
              "value": "Value tu push for the quantity",
              "values": "Vectorial value tu push for the quantity",
              "step": "Step at which the data is generated",
              "is_float": "0 if the quantity is a float 1 other why"}
    )

    params = parser.parseBDParameters()

    if("runid" not in params):
        raise Exception("The run id should be set")
    if("quantity_id" not in params):
        raise Exception("The quantity id should be set")
    if("value" not in params and "values" not in params):
        raise Exception("The value should be set")

    is_vector = False
    if("values" in params):
        is_vector = True
        if("value" in params):
            raise Exception(
                "You cannot define values and value at the same time")

    base = BD.Base(**params)

    if ("runid" in params):
        if "run_constraints" not in params:
            params["run_constraints"] = []
            params["run_constraints"].append("id = " + str(params["runid"]))

    runSelector = BD.RunSelector(base)
    run_list = runSelector.selectRuns(params, params)

    if (not len(run_list) == 1):
        raise Exception("No or too many runs selected")

    r, j = run_list[0]

    if params["truerun"] is True:
        if is_vector is False:
            r.pushScalarQuantity(
                params["value"], params["step"], params["quantity_id"],
                params["is_float"] is False)
        else:
            r.pushVectorQuantity(
                params["values"], params["step"], params["quantity_id"],
                params["is_float"] is False)
        base.commit()


if __name__ == '__main__':
    main()
