#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- py-which-shell: "python"; -*-
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################
from . import run
from . import job
from . import selector
from . import constraints as BDconstraints
################################################################


class RunSelector(selector.Selector):
    """
    """

    def selectRuns(self, constraints,
                   sort_by=None,
                   quiet=False):

        run_constraints = BDconstraints.BDconstraints(self.base,
                                                      'runs.job_id = jobs.id')
        run_constraints += constraints

        run_list = self.select([run.Run, job.Job],
                               constraints=run_constraints,
                               sort_by=sort_by)

        if quiet is False:
            if not run_list:
                print("no runs found")
            print("Selected runs are: " + str([r[0].id for r in run_list]))
            print("Selected jobs are: " + str([r[0]["job_id"]
                                               for r in run_list]))

        return run_list

    def __init__(self, base):
        selector.Selector.__init__(self, base)

################################################################


__all__ = ["RunSelector"]
