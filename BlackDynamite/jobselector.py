#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- py-which-shell: "python"; -*-
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################
from . import selector
from . import job
################################################################


class JobSelector(selector.Selector):
    """
    """

    def selectJobs(self, constraints=None, sort_by=None, quiet=False):

        job_list = self.select(job.Job, constraints=constraints,
                               sort_by=sort_by)

        if quiet is False:
            if not job_list:
                print("no jobs found")
            print("Selected jobs are: " + str([j.id for j in job_list]))
        return job_list

    def __init__(self, base):
        selector.Selector.__init__(self, base)

################################################################


__all__ = ["JobSelector"]
